<?php
//https://github.com/codeguy/Slim-Views

session_start();
require 'vendor/autoload.php';
$app = new \Slim\Slim(array(
	//para cargar el twig
    'view' => new \Slim\Views\Twig()
));

// configuraciones de slim
$app->config(array(
	'debug' => true,
	'templates.path' => 'views'
));

//configuraiciones de twig
$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);

//carga de extenciones de twig ---urlFor siteUrl baseUrl
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);


//Hooks
$app->hook('slim.before', function() use($app){
	$app->view()->appendData(array('baseUrl' => WEB));
});

define('SPECIALCONSTANT', true);
//define('WEB', 'http://192.168.0.10/fisioadmin');
define('WEB', 'http://localhost/fisioadmin');
//define('WEB', 'http://admin.fisiostetik.com.py');

//Conexion
require_once 'conexion.php';
require_once 'controllers/login.php';
require_once 'controllers/dashboard.php';
require_once 'controllers/pacientes.php';
require_once 'controllers/user.php';
require_once 'controllers/events.php';


/*Redireccion 404
$app->notFound(function() use($app) {
	$app->render('404.twig');
});*/

$app->run();

?>