<?php

/*----------Index Calendar----------*/
$app->get('/calendario', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Events.php';
        require_once 'models/Selectores.php';

        $calendario = new Events();
        //$citas = $calendario -> citas();
        //$dia = $calendario->verDia();
        //$consultar = $calendario->verConsultar();

        $selector = new Selectores();
        $pacientes = $selector->cargarPacientes();
        $userAr = $selector->returnRol();

        $app->render('calendario/citas.html.twig', array(
             'pacientes' => $pacientes, 'user' => $userAr ));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('citas');