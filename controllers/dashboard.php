<?php
/*----------Index Dashboard----------*/
$app->get('/dashboard', function() use($app){

    //render cuando inicia sesion
    if (!empty($_SESSION['session'])) {
        //require_once 'models/Dashboard.php';
        require_once 'models/Selectores.php';

        //$dash = new Dashboard();
        $selector = new Selectores();
        $userAr = $selector->returnRol();

        //array_push($dashAr, "saludo", "hola");

        $app->render('dashboard.html.twig', array('user' => $userAr));
    } else {
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }

})->name('dashboard');
