<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 24/08/2016
 * Time: 03:25 PM
 */
class User{

    private $iduser;
    private $usuario;
    private $contrasena;
    private $nombre;
    private $avatar;
    private $rol;

    /**
     * @return mixed
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * @param mixed $rol
     */
    public function setRol($rol)
    {
        $this->rol = $rol;
    }

    /**
     * @return mixed
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param mixed $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }

    /**
     * @return mixed
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * @param mixed $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * @return mixed
     */
    public function getContrasena()
    {
        return $this->contrasena;
    }

    /**
     * @param mixed $contrasena
     */
    public function setContrasena($contrasena)
    {
        $this->contrasena = $contrasena;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function verUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT * FROM user;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertarUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO user (usuario,contrasena,nombre,avatar,rol) VALUES (:usuario,:pass,:nombre,:avatar,:rol)");
        $query->execute(array(':usuario' => $this->getUsuario(),
            ':pass' => $this->getContrasena(),
            ':nombre' => $this->getNombre(),
            ':avatar' => $this->getAvatar(),
            ':rol' => $this->getRol()));
        $conexion = null;
    }

    public function selectUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT * FROM user WHERE iduser = :id;");
        $query->execute(array('id' => $this->getIduser() ));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function updateUser(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE user SET usuario=:usuario, nombre=:nombre, contrasena=:contrasena WHERE iduser= :id;");
        $query->execute(array('usuario' => $this->getUsuario(),
            'nombre' => $this->getNombre(),
            'contrasena' => $this->getContrasena(),
            'id' => $this->getIduser()));
        return $query->rowCount();
        $conexion = null;
    }
}