<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 20/07/2016
 * Time: 08:05 PM
 */
class Pacientes{

    private $idpacientes;
    private $nombre;
    private $telefono;
    private $nacimiento;
    private $ciudad;
    private $email;
    private $sexo;
    private $estatura;
    private $idtratamiento;
    private $documento;
    private $direccion;
    private $ruc;
    private $apellidos;
    private $celular;

    /**
     * @return mixed
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @param mixed $documento
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    /**
     * @return mixed
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * @param mixed $direccion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }

    /**
     * @return mixed
     */
    public function getRuc()
    {
        return $this->ruc;
    }

    /**
     * @param mixed $ruc
     */
    public function setRuc($ruc)
    {
        $this->ruc = $ruc;
    }

    /**
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param mixed $apellidos
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;
    }

    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }

    /**
     * @param mixed $celular
     */
    public function setCelular($celular)
    {
        $this->celular = $celular;
    }

    /**
     * @return mixed
     */
    public function getIdtratamiento()
    {
        return $this->idtratamiento;
    }

    /**
     * @param mixed $idtratamiento
     */
    public function setIdtratamiento($idtratamiento)
    {
        $this->idtratamiento = $idtratamiento;
    }

    /**
     * @return mixed
     */
    public function getIdpacientes()
    {
        return $this->idpacientes;
    }

    /**
     * @param mixed $idpacientes
     */
    public function setIdpacientes($idpacientes)
    {
        $this->idpacientes = $idpacientes;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return mixed
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param mixed $telefono
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }

    /**
     * @return mixed
     */
    public function getNacimiento()
    {
        return $this->nacimiento;
    }

    /**
     * @param mixed $nacimiento
     */
    public function setNacimiento($nacimiento)
    {
        $this->nacimiento = $nacimiento;
    }

    /**
     * @return mixed
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param mixed $ciudad
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getEstatura()
    {
        return $this->estatura;
    }

    /**
     * @param mixed $estatura
     */
    public function setEstatura($estatura)
    {
        $this->estatura = $estatura;
    }


    public function verTodos($limit){
        $conexion = new Conexion();
        $query = $conexion->prepare("select p.idpacientes, p.nombre, p.apellidos, p.documento, p.nacimiento, p.celular, p.telefono, p.email, p.ciudad, t.tratamiento, TIMESTAMPDIFF(YEAR,nacimiento,CURDATE()) AS edad
                                            from pacientes p, tratamientos t
                                            where p.idtratamientos=t.idtratamientos
                                            ORDER BY p.idpacientes DESC LIMIT $limit;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }


    public function insertarPaciente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO pacientes(nombre,apellidos, documento, nacimiento, celular, telefono, email, direccion, ciudad, sexo, idtratamientos)
                                    VALUES(:nombre,:apellido,:documento, :nacimiento, :celular, :telefono, :email, :direccion, :ciudad, :sexo, :idtratamientos);");
        $query->execute(array('nombre' => $this->getNombre(),
            'apellido' => $this->getApellidos(),
            'documento' => $this->getDocumento(),
            'nacimiento' => $this->getNacimiento(),
            'celular' => $this->getCelular(),
            'telefono' => $this->getTelefono(),
            'email' => $this->getEmail(),
            'direccion' => $this->getDireccion(),
            'ciudad' => $this->getCiudad(),
            'sexo' => $this->getSexo(),
            'idtratamientos' =>$this->getIdtratamiento()));
        $id = $conexion->lastInsertId();
        $conexion = null;
        return $id;
    }

    public function selectPaciente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idpacientes, nombre, apellidos, DATE_FORMAT(nacimiento,'%d-%m-%Y') nacimiento, documento,  celular, telefono, email, direccion, ciudad, sexo, idtratamientos FROM pacientes WHERE idpacientes = :id;");
        $query->execute(array('id' => $this->getIdpacientes() ));
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

    public function updatePaciente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("UPDATE pacientes SET nombre=:nombre, apellidos=:apellidos, nacimiento=:nacimiento, documento=:documento,
                                              telefono=:telefono,celular=:celular, ciudad=:ciudad, email=:email, sexo=:sexo, direccion=:direccion, idtratamientos=:tratamiento
                                              WHERE idpacientes= :id;");
        $query->execute(array('nombre' => $this->getNombre(),
            'apellidos' => $this->getApellidos(),
            'nacimiento' => $this->getNacimiento(),
            'documento' => $this->getDocumento(),
            'telefono' => $this->getTelefono(),
            'celular' => $this->getCelular(),
            'ciudad' => $this->getCiudad(),
            'email' => $this->getEmail(),
            'sexo' => $this->getSexo(),
            'direccion' => $this->getDireccion(),
            'tratamiento' => $this->getIdtratamiento(),
            'id' => $this->getIdpacientes()));
        $conexion = null;
        return $query->rowCount();
    }

    public function deletePaciente(){
        $conexion = new Conexion();
        $query = $conexion->prepare("DELETE FROM pacientes WHERE idpacientes= :id;");
        $query->execute(array('id' => $this->getIdpacientes()));
        $conexion = null;
        return $query->rowCount();
    }

    public function searchPatien($paciente){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT p.idpacientes, p.nombre, p.apellidos, p.documento, p.nacimiento, p.celular, p.telefono, p.email, p.ciudad, t.tratamiento, TIMESTAMPDIFF(YEAR,nacimiento,CURDATE()) AS edad
                                            FROM pacientes p, tratamientos t
                                            WHERE p.idtratamientos = t.idtratamientos
                                            AND p.apellidos LIKE '%".$paciente."%'
                                            ORDER BY p.nombre");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $conexion = null;
        return $result;
    }

}