<?php
/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 14/07/2016
 * Time: 03:31 PM
 */

/*----------Index Pacientes----------*/
$app->get('/pacientes', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Pacientes.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $pacientes = new Pacientes();
        $result = $pacientes->vertodos(100);
        //var_dump($result);

        $app->render('pacientes/pacientes.html.twig', array(
            'pacientes' => $result, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('pacientes');

/*----------Insertar Pacientes----------*/
$app->post('/pacientes/nuevo', function() use($app){

    require_once 'models/Pacientes.php';
    require_once 'models/Abm.php';

    $pacientes = new Pacientes();
    $request = $app->request;

    $pacientes->setNombre($request->post('nombre'));
    $pacientes->setApellidos($request->post('apellido'));
    $pacientes->setDocumento($request->post('documento'));

    //Nacimiento paciente
    $nacimiento=$request->post('nacimiento');
    if(!empty($nacimiento)){
        $date = date_create($nacimiento);
        $dateFormat = date_format($date,"Y/m/d");
        $pacientes->setNacimiento($dateFormat);
    }else{
        $pacientes->setNacimiento(date("Y-m-d"));
    }

    $pacientes->setSexo($request->post('sexo'));
    $pacientes->setCelular($request->post('celular'));
    $pacientes->setTelefono($request->post('telefono'));
    $pacientes->setEmail($request->post('email'));
    $pacientes->setDireccion($request->post('direccion'));
    $pacientes->setCiudad(strtolower($request->post('ciudad')));
    $pacientes->setIdtratamiento($request->post('tratamiento'));

    /*echo ($pacientes->getNombre()."/");
    echo ($pacientes->getApellidos()."/");
    echo ($pacientes->getDocumento()."/");
    echo ($pacientes->getNacimiento()."/");
    echo ($pacientes->getSexo()."/");
    echo ($pacientes->getCelular()."/");
    echo ($pacientes->getTelefono()."/");
    echo ($pacientes->getEmail()."/");
    echo ($pacientes->getDireccion()."/");
    echo ($pacientes->getCiudad()."/");
    echo ($pacientes->getIdtratamiento());*/


    $insert=$pacientes->insertarPaciente();
    if($insert){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Insertado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Paciente ya registrado');
    }

    $app->redirect($app->urlFor('pacientes'));


})->name('nuevo-paciente');

/*----------Modificar Pacientes----------*/
$app->get('/update/paciente/:id', function($id) use($app){
    if(!empty($_SESSION['session'])){
        require_once 'models/Pacientes.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $pacientes = new Pacientes();
        $pacientes->setIdpacientes($id);
        $paciente = $pacientes->selectPaciente();

        $app->render('pacientes/update-paciente.html.twig', array( 'paciente' => $paciente, 'user' => $userAr));
    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('up-paciente');

$app->post('/paciente/update', function() use($app){

    require_once 'models/Pacientes.php';
    require_once 'models/Abm.php';

    $pacientes = new Pacientes();
    $abm = new Abm();

    $request = $app->request;
    $pacientes->setIdpacientes($request->post('id'));
    $pacientes->setNombre(strtoupper($request->post('nombre')));
    $pacientes->setApellidos(strtoupper($request->post('apellido')));
    $pacientes->setDocumento($request->post('documento'));

    $nacimiento=$request->post('nacimiento');
    if(!empty($nacimiento)){
        $date = date_create($nacimiento);
        $dateFormat = date_format($date,"Y/m/d");
        $pacientes->setNacimiento($dateFormat);
    }else{
        $pacientes->setNacimiento(date("Y-m-d"));
    }

    $pacientes->setSexo($request->post('sexo'));
    $pacientes->setCelular($request->post('celular'));
    $pacientes->setTelefono($request->post('telefono'));
    $pacientes->setEmail($request->post('email'));
    $pacientes->setDireccion($request->post('direccion'));
    $pacientes->setCiudad(strtolower($request->post('ciudad')));
    $pacientes->setIdtratamiento($request->post('tratamiento'));

    $modificado=$pacientes->updatePaciente();

    if($modificado){
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Paciente modificado Correctamente');

        $abm->setPaciente($pacientes->getNombre());
        $abm->setTabla('pacientes');
        $abm->setIduser($request->post('user'));
        $detalle = "MODIFICADO: ".strtolower($request->post('detalle'));
        $abm->setDetalle($detalle);
        $abm->insertarAbm();
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'No se pudo modificar');
    }

    $app->redirect($app->urlFor('pacientes'));

})->name('update-paciente');

/*------------Eliminar Paciente-------------*/
$app->post('/paciente/delete', function() use($app){

    require_once 'models/Pacientes.php';
    require_once 'models/Abm.php';
    $pacientes = new Pacientes();
    $abm = new Abm();
    $request = $app->request;
    $pacientes->setIdpacientes($request->post('codigo-paciente'));
    $pacientes->setNombre($request->post('nombre-paciente'));
    $eliminado = $pacientes->deletePaciente();

    if($eliminado > 0 ){
        $abm->setPaciente(strtolower($pacientes->getNombre()));
        $abm->setTabla('pacientes');
        $abm->setIduser($request->post('user'));
        $detalle = "ELIMINADO: ".strtolower($request->post('detalle-del'));
        $abm->setDetalle($detalle);
        $abm->insertarAbm();
        $app->flash('content', 'alert-success');
        $app->flash('mensaje', 'Eliminado Correctamente');
    }else{
        $app->flash('content', 'alert-danger');
        $app->flash('mensaje', 'Error: ya existen datos con este paciente!');
    }

    $app->redirect($app->urlFor('pacientes'));

})->name('delete-paciente');


/*----------Reporte Pacientes----------*/
$app->get('/reporte-paciente', function() use($app){
    //si hay sesion abierta
    if(!empty($_SESSION['session'])){
        require_once 'models/Pacientes.php';
        require_once 'models/Selectores.php';

        $selector = new Selectores();
        $userAr = $selector->returnRol();

        $pacientes = new Pacientes();
        $pacientesAr = $pacientes->vertodos(10000);

        $app->render('pacientes/reporte-pacientes.html.twig', array(
            'pacientes' => $pacientesAr, 'user' => $userAr));

    }else{
        //si no hay redirecciona al login
        $app->redirect($app->urlFor('login'));
    }
})->name('reporte-paciente');

/*----------Buscar Pacientes----------*/
$app->post('/busqueda-paciente', function() use($app){

    require_once 'models/Selectores.php';
    require_once 'models/Pacientes.php';

    $request = $app->request;
    $paciente = $request->post('buscar');

    $pacientes = new Pacientes();
    $selector = new Selectores();
    $userAr = $selector->returnRol();
    $result = $pacientes->searchPatien($paciente);

    $app->render('pacientes/pacientes.html.twig', array('pacientes' => $result, 'user' => $userAr));

})->name('search-pacientes');
