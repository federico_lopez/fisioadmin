<?php

class Abm{

    private $idabm;
    private $tabla;
    private $paciente;
    private $iduser;
    private $detalle;

    /**
     * @return mixed
     */
    public function getIdabm()
    {
        return $this->idabm;
    }

    /**
     * @param mixed $idabm
     */
    public function setIdabm($idabm)
    {
        $this->idabm = $idabm;
    }

    /**
     * @return mixed
     */
    public function getTabla()
    {
        return $this->tabla;
    }

    /**
     * @param mixed $tabla
     */
    public function setTabla($tabla)
    {
        $this->tabla = $tabla;
    }

    /**
     * @return mixed
     */
    public function getPaciente()
    {
        return $this->paciente;
    }

    /**
     * @param mixed $paciente
     */
    public function setPaciente($paciente)
    {
        $this->paciente = $paciente;
    }

    /**
     * @return mixed
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * @param mixed $iduser
     */
    public function setIduser($iduser)
    {
        $this->iduser = $iduser;
    }

    /**
     * @return mixed
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * @param mixed $detalle
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;
    }



    public function sentencias($sentencia){
        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetch(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function insertarAbm(){
        $conexion = new Conexion();
        $query = $conexion->prepare("INSERT INTO abm(tabla,detalle,paciente,iduser)
                                    VALUES(:tabla, :detalle, :paciente, :iduser);");
        $query->execute(array('tabla' => $this->getTabla(),
            'detalle' => $this->getDetalle(),
            'paciente' => $this->getPaciente(),
            'iduser' =>$this->getIduser()));
        $conexion = null;
    }


}