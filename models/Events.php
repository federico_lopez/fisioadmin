<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 26/01/2017
 * Time: 10:08 AM
 */
class Events{
    private $id;
    private $start;
    private $end;
    private $title;
    private $idbox;
    private $idpaciente;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param mixed $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param mixed $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getIdbox()
    {
        return $this->idbox;
    }

    /**
     * @param mixed $idbox
     */
    public function setIdbox($idbox)
    {
        $this->idbox = $idbox;
    }

    /**
     * @return mixed
     */
    public function getIdpaciente()
    {
        return $this->idpaciente;
    }

    /**
     * @param mixed $idpaciente
     */
    public function setIdpaciente($idpaciente)
    {
        $this->idpaciente = $idpaciente;
    }

    public function verTodos(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT e.id, e.title, e.start, e.end, CONCAT(UPPER(p.nombre), ' - ', p.telefono) 'paciente', b.color 'className'
                                    FROM events e, pacientes p, box b
                                    WHERE e.idpacientes = p.idpacientes AND e.idbox = b.idbox");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    /*---------Consultas CITAS-----------*/
    public function citas(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT e.id , e.title 'servicio', e.start 'desde', e.end 'hasta', CONCAT(UPPER(p.nombre), ' - ', p.telefono) 'paciente', b.boxnro 'box', b.hexadecimal
                                    FROM events e, pacientes p, box b
                                    WHERE e.idpacientes = p.idpacientes AND e.idbox = b.idbox");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }



}