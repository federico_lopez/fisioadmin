<?php

class Conexion extends PDO
{
    private $bd = 'mysql';
    private $host = 'localhost';
   // private $name = 'fisioste_fisioterapia';
    private $name = 'fisioste_fisioterapia';
    private $user = 'root';
    private $pass = '123456789';

    //private $user = 'fisioste_fisio';
    //private $pass = 'Fisioterapia123';
    public function __construct()
    {
        //Sobreescribo el método constructor de la clase PDO.
        try {
            parent::__construct($this->bd . ':host=' . $this->host . ';dbname=' . $this->name, $this->user, $this->pass);
        } catch (PDOException $e) {
            echo 'PDO exception: ' . $e->getMessage();
            exit;
        }
    }
}
