<?php

/**
 * Created by PhpStorm.
 * User: FedeXavier
 * Date: 03/05/2016
 * Time: 01:12 PM
 */
class Selectores
{

    public function cargarPacientes(){
        $conexion = new Conexion();
        $query = $conexion->prepare("SELECT idpacientes, nombre FROM pacientes order by nombre asc;");
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }

    public function returnRol(){
        $userAr['iduser'] = $_SESSION['session']['iduser'];
        $userAr['idrol'] = $_SESSION['session']['rol'];
        $userAr['imagen'] = $_SESSION['session']['avatar'];
        $userAr['user'] = $_SESSION['session']['usuario'];
        $userAr['nombre'] = $_SESSION['session']['nombre'];
        return $userAr;
    }

    public function sentenciaAll($sentencia){
        $conexion = new Conexion();
        $query = $conexion->prepare($sentencia);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
        $conexion = null;
    }


}